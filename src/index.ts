import paper, { view } from "paper";
import JSZip from "jszip";
import { saveAs } from 'file-saver';
import kdTree from "kd-tree-javascript";

let distance = (a: paper.Point, b: paper.Point) => {
    var dx = a.x - b.x;
    var dy = a.y - b.y;
    return dx * dx + dy * dy;
}

let brushCanvas = document.createElement('canvas');
brushCanvas.width = 45;
brushCanvas.height = 45;
let brushContext: CanvasRenderingContext2D = brushCanvas.getContext('2d') as any;

let paperCanvas = document.getElementById("paper") as HTMLCanvasElement;
paper.install(window);
paper.setup(paperCanvas);
paper.view.element.getContext('2d');

let mainProject = paper.project;

let brushImages: HTMLImageElement[] = [];
for (let i = 1; i <= 5; i++) {
    var image = new Image();
    image.src = './static/brushes/brush' + i + '.png';
    brushImages.push(image);
}
mainProject.activate();
let random = (min: number, max: number) => {
    return min + Math.random() * (max - min);
}

let randomPoint = (min: number, max: number) => {
    let x = random(min, max);
    let y = random(min, max);
    return new paper.Point(x, y);
}

let pathsAndSymbols = new paper.Group();

let draw = (bounds: paper.Rectangle) => {
    pathsAndSymbols.removeChildren();
    let paths = new paper.Group();
    let symbols = new paper.Group();
    pathsAndSymbols.addChild(paths);
    pathsAndSymbols.addChild(symbols);
    for (let i = 0; i < 10; i++) {
        let color = paper.Color.random();
        let path = new paper.Path();
        path.strokeColor = color;
        path.strokeWidth = 2;
        paths.addChild(path);
        for (let j = 0; j < 10; j++) {
            let x = Math.random() * bounds.width;
            let y = Math.random() * bounds.height;
            path.add(new paper.Point(x, y));
            if (Math.random() > 0.5) {
                path.lastSegment.handleIn = randomPoint(-200, 200);
            }
            if (Math.random() > 0.5) {
                path.lastSegment.handleOut = randomPoint(-200, 200);
            }
        }
        let brushes: paper.SymbolDefinition[] = [];
        for (let brushImage of brushImages) {
            brushContext.clearRect(0, 0, 45, 45);
            brushContext.globalCompositeOperation = "source-over";
            brushContext.fillStyle = color.toCSS(false);
            brushContext.fillRect(0, 0, 45, 45);
            brushContext.globalCompositeOperation = "destination-in";
            brushContext.drawImage(brushImage, 0, 0);
            let brushRaster = new paper.Raster(new paper.Size(45, 15));
            brushRaster.canvas.getContext('2d')?.drawImage(brushCanvas, 0, 0, 45, 15);
            brushes.push(new paper.SymbolDefinition(brushRaster));
            brushRaster.remove();
        }

        for (let j = 0; j < path.length; j += 10) {
            let symbol = new paper.SymbolItem(brushes[Math.floor(Math.random() * brushes.length)]);
            symbol.position = path.getPointAt(j);
            symbol.rotation = path.getTangentAt(j).angle + random(-5, 5);
            symbol.opacity = random(0, 1);
            symbol.scale(0.25 + Math.random() * 0.75);
            symbols.addChild(symbol);
        }
    }
    paths.visible = false;
    pathsAndSymbols.fitBounds(bounds);
    pathsAndSymbols.bringToFront();
    return pathsAndSymbols;
}

let drawClean = (bounds: paper.Rectangle, pathsAndSymbols: paper.Group)=> {
    let paths = pathsAndSymbols.children[0] as paper.Group;
    paths.visible = true;
    let symbols = pathsAndSymbols.children[1] as paper.Group;
    symbols.visible = false;
};

let a4rectangle = new paper.Rectangle(0, 0, 0, 0);
let a4shape = new paper.Path.Rectangle(a4rectangle);

let updateA4Shape = () => {
    a4shape.remove();
    a4shape = new paper.Path.Rectangle(a4rectangle);
    a4shape.strokeColor = 'green' as any;
    a4shape.strokeWidth = 1;
};

paper.view.onMouseDown = (event: paper.MouseEvent) => {
    // draw();
    a4rectangle.topLeft = event.point;
    updateA4Shape();
}
paper.view.onMouseDrag = (event: paper.MouseEvent) => {
    a4rectangle.bottomRight = event.point;
    updateA4Shape();
}
paper.view.onMouseUp = (event: paper.MouseEvent) => {
    a4rectangle.bottomRight = event.point;
    updateA4Shape();
}

let randn_bm = (): number => {
    let u = 0, v = 0;
    while (u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while (v === 0) v = Math.random();
    let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) return randn_bm(); // resample between 0 and 1
    return num;
}

let drawingRaster: paper.Raster = new paper.Raster('./static/marilyn.jpg')

drawingRaster.onLoad = () => {
    drawingRaster.fitBounds(paper.view.bounds.expand(-50));
    // drawRaster();
    drawingRaster.opacity = 0;
};

let randomNormalDistribution = (sigma = 1, mu = 0) => {
    let x = Math.random();
    let xmmos = (x - mu) / sigma;
    return (1 / (sigma * Math.sqrt(2 * Math.PI))) * Math.exp(-0.5 * xmmos * xmmos);
}

let rasterToView = (pixel: paper.Point, r: paper.Raster = drawingRaster) => {
    let dx = (pixel.x / r.width) * r.bounds.width;
    let dy = (pixel.y / r.height) * r.bounds.height;
    return r.bounds.topLeft.add(new paper.Point(dx, dy));
}

let viewToRaster = (point: paper.Point, r: paper.Raster = drawingRaster) => {
    let dx = ((point.x - r.bounds.left) / r.bounds.width) * r.width;
    let dy = ((point.y - r.bounds.top) / r.bounds.height) * r.height;
    return new paper.Point(dx, dy);
}

let rasterCircles = new paper.Group();
let rasterLines = new paper.Group();

// let drawRaster = ()=> {
//     rasterCircles.removeChildren();
//     let histogram: number[]= [];
//     let histogramToPixels: paper.Point[][] = [];
//     for(let i=0 ; i<255 ; i++) {
//         histogram.push(0);
//         histogramToPixels.push([]);
//     }
//     for(let y=0 ; y<drawingRaster.height ; y++) {
//         for(let x=0 ; x<drawingRaster.width ; x++) {
//             let color = drawingRaster.getPixel(x, y).convert('gray');
//             let index = Math.floor(color.gray*255);
//             histogram[index]++;
//             histogramToPixels[index].push(new paper.Point(x, y));
//         }
//     }
//     for(let i=0 ; i<100000 ; i++) {
//         let r = Math.pow(Math.random(), 100);
//         let p = Math.floor(r * 255);
//         if(histogramToPixels[p].length == 0) {
//             continue;
//         }
//         let pixel = histogramToPixels[p][Math.floor(Math.random()*histogramToPixels[p].length)];
//         let circle = new paper.Path.Circle(rasterToView(pixel), 1);
//         circle.fillColor = 'black' as any;
//         rasterCircles.addChild(circle);
//     }
// };

let drawRaster = () => {
    rasterCircles.removeChildren();
    let points: paper.Point[] = [];
    while (rasterCircles.children.length < 10000) {
        let px = random(drawingRaster.bounds.left, drawingRaster.bounds.right);
        let py = random(drawingRaster.bounds.bottom, drawingRaster.bounds.top);
        let p = new paper.Point(px, py);
        let pixel = viewToRaster(p);
        let color = drawingRaster.getPixel(pixel).convert('gray');
        let n = Math.random();
        if (n * n * n > color.gray * 1.5) {
            let circle = new paper.Path.Circle(p, 1);//1.2 * (1 - color.gray));
            circle.fillColor = 'black' as any;
            rasterCircles.addChild(circle);
            points.push(p);
        }
    }
    (window as any).rasterCircle = rasterCircles;
    rasterCircles.opacity = 0.1;
    rasterLines.removeChildren();
    (window as any).rasterLines = rasterCircles;
    var tree = new kdTree.kdTree(points, distance, ["x", "y"]);
    // let color = paper.Color.random().toCSS(false);
    let color = 'black';

    let brushes: paper.SymbolDefinition[] = [];

    for (let brushImage of brushImages) {
        brushContext.clearRect(0, 0, 45, 45);
        brushContext.globalCompositeOperation = "source-over";
        brushContext.fillStyle = color;
        brushContext.fillRect(0, 0, 45, 45);
        brushContext.globalCompositeOperation = "destination-in";
        brushContext.drawImage(brushImage, 0, 0);
        let brushRaster = new paper.Raster(new paper.Size(45, 15));
        brushRaster.canvas.getContext('2d')?.drawImage(brushCanvas, 0, 0, 45, 15);
        brushes.push(new paper.SymbolDefinition(brushRaster));
    }

    for (let i = 0; i < 200; i++) {
        let p = points[Math.floor(Math.random() * points.length)];
        let path = new paper.Path();
        path.strokeWidth = 1;
        path.strokeColor = 'black' as any;
        path.opacity = 0.2;
        rasterLines.addChild(path);
        for (let i = 0; i < 600; i++) {
            tree.remove(p);
            let ps = tree.nearest(p, 10);
            if (ps[0] && ps[0][0]) {
                p = ps[0][0];
                path.add(p);
            } else {
                p = points[Math.floor(Math.random() * points.length)];
            }
        }
        path.smooth();
        path.opacity = 0;
        for (let j = 0; j < path.length; j += 10) {
            let symbol = new paper.SymbolItem(brushes[Math.floor(Math.random() * brushes.length)]);
            symbol.position = path.getPointAt(j);
            symbol.rotation = path.getTangentAt(j).angle + random(-5, 5);
            symbol.opacity = random(0, 1);
            symbol.scale(0.25 + Math.random() * 0.75);
            pathsAndSymbols.addChild(symbol);
        }
    }
};

let onDocumentDrag = (event: any) => {
    event.preventDefault();
}

let onDocumentDrop = (event: any) => {

    event.preventDefault();
    if (event.dataTransfer?.files) {
        var file = event.dataTransfer.files[0];
        var reader = new FileReader();

        reader.onload = function (event: any) {
            var image = document.createElement('img');
            image.onload = function () {
                drawingRaster?.remove();
                drawingRaster = new paper.Raster(image);
                drawingRaster.fitBounds(paper.view.bounds.expand(-50));
                drawRaster();
                drawingRaster.opacity = 0.3;
            };
            image.src = event.target.result;
        };
        reader.readAsDataURL(file);
    }
};

document.addEventListener('drop', onDocumentDrop, false);
document.addEventListener('dragover', onDocumentDrag, false);
document.addEventListener('dragleave', onDocumentDrag, false);

// Load A4

let rectangles: any = {};
let rectanglesStored = localStorage.getItem('rectangles');
if (rectanglesStored != null && rectanglesStored != '') {
    rectangles = JSON.parse(rectanglesStored);
}
let a4Rasters = new paper.Group();
let currentRaster = 0;

for (let i = 1; i <= 25; i++) {
    let paperName = './static/A4/landscapes/paper' + i + '.jpg'
    let raster = new paper.Raster(paperName);
    raster.onLoad = () => raster.fitBounds(paper.view.bounds);
    raster.visible = false;
    raster.data.name = paperName;
    a4Rasters.addChild(raster);
    if (!rectangles.hasOwnProperty(paperName)) {
        rectangles[paperName] = {};
    }
}
for (let i = 1; i <= 10; i++) {
    let paperName = './static/A4/portraits/paper' + i + '.jpg'
    let raster = new paper.Raster(paperName);
    raster.onLoad = () => raster.fitBounds(paper.view.bounds);
    raster.visible = false;
    raster.data.name = paperName;
    a4Rasters.addChild(raster);
    if (!rectangles.hasOwnProperty(paperName)) {
        rectangles[paperName] = {};
    }
}
a4Rasters.children[currentRaster].visible = true;

function download(content: string, fileName: string, contentType: string) {
    var a = document.createElement("a");
    var file = new Blob([content], { type: contentType });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}

let rectangleRasterToView = (rectangle: paper.Rectangle, raster: paper.Raster) => {
    return new paper.Rectangle(rasterToView(rectangle.topLeft, raster), rasterToView(rectangle.bottomRight, raster));
}

let rectangleViewToRaster = (rectangle: paper.Rectangle, raster: paper.Raster) => {
    return new paper.Rectangle(viewToRaster(rectangle.topLeft, raster), viewToRaster(rectangle.bottomRight, raster));
}

let paperRectangleToObject = (rectangle: paper.Rectangle, raster: paper.Raster) => {
    let rasterRectangle = rectangleViewToRaster(rectangle, raster);
    return { x: rasterRectangle.left, y: rasterRectangle.top, width: rasterRectangle.width, height: rasterRectangle.height };
}
let objectToPaperRectangle = (rectangle: any, raster: paper.Raster) => {
    let viewRectangle = new paper.Rectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    return rectangleRasterToView(viewRectangle, raster);
}

let gridScale = 0.5;

let sliceImage = (raster: paper.Raster)=> {
    let smallestSize = raster.width < raster.height ? raster.width : raster.height;
    let size = smallestSize * gridScale;

    let nRectanglesWidth = Math.ceil(raster.width / size);
    let nRectanglesHeight = Math.ceil(raster.height / size);

    let stepX = (raster.width - size) / nRectanglesWidth;
    let stepY = (raster.height - size) / nRectanglesHeight;

    let rasterParts = new paper.Group()

    for(let nx=0 ; nx<=nRectanglesWidth ; nx++) {
        for(let ny=0 ; ny<=nRectanglesHeight ; ny++) {
            let rectangle = new paper.Rectangle(nx * stepX, ny * stepY, size, size);
            rasterParts.addChild(raster.getSubRaster(rectangle));
        }
    }
    return rasterParts;
};

let exportImage = (group: paper.Group, images: JSZip, n: number, jpg=false)=> {
    let raster = group.rasterize(undefined, false);
    if(raster.width > raster.height && raster.height > 1000) {
        raster.width *= 1000 / raster.height;
        raster.height = 1000;
    } else if(raster.height > raster.width && raster.width > 1000) {
        raster.height *= 1000 / raster.width;
        raster.width = 1000;
    }
    raster.remove();
    let rasterParts = sliceImage(raster);
    rasterParts.remove();
    // download(content, 'image.png', 'image/png');
    let i=0;
    for(let rasterPart of rasterParts.children) {
        let rp: any = rasterPart;
        let content = jpg ? rp.toDataURL('image/jpeg', 0.8) : rp.toDataURL();
        images.file("image" + n + "-" + i + (jpg ? ".jpg" :  ".png"), content.split(',')[1], {base64: true});
        i++;
    }
};

let exportImages = ()=> {
    var zip = new JSZip();
    var images = zip.folder("images") as JSZip;
    var labels = zip.folder("labels") as JSZip;
    currentRaster = 0;
    a4Rasters.children[currentRaster].visible = false;
    let background = new paper.Path.Rectangle(a4Rasters.children[currentRaster].bounds);
    background.fillColor = 'white' as any;
    let nImagePerA4 = 10;
    while(currentRaster < a4Rasters.children.length) {
        console.log(a4Rasters.children[currentRaster].data.name);
        a4rectangle = objectToPaperRectangle(rectangles[a4Rasters.children[currentRaster].data.name], a4Rasters.children[currentRaster] as paper.Raster);
        updateA4Shape();
        a4Rasters.children[currentRaster].visible = true;
        a4shape.visible = false;
        background = new paper.Path.Rectangle(a4Rasters.children[currentRaster].bounds);
        background.fillColor = 'white' as any;
        for(let i=0 ; i<nImagePerA4 ; i++) {
            let group = new paper.Group();
            group.addChild(background);
            let a4Raster = a4Rasters.children[currentRaster].clone()
            group.addChild(a4Raster);
            a4Raster.visible = false;
            let pathsAndSymbols = draw(a4rectangle).clone();
            group.addChild(pathsAndSymbols);
            exportImage(group, images, currentRaster*nImagePerA4 + i, true);
            a4Raster.visible = false;
            drawClean(a4rectangle, pathsAndSymbols);
            exportImage(group, labels, currentRaster*nImagePerA4 + i);
            group.remove();
        }

        currentRaster++;
    }
    zip.generateAsync({type:"blob"})
    .then(function(content) {
        // see FileSaver.js
        saveAs(content, "groundTruth.zip");
    });
};

let onKeyDown = (event: KeyboardEvent) => {
    switch (event.key) {
        case "Down":
        case "ArrowDown":
            break;
        case "Up":
        case "ArrowUp":
            break;
        case "Left":
        case "ArrowLeft":
            if (currentRaster > 0) {
                a4Rasters.children[currentRaster].visible = false;
                currentRaster--;
                console.log(a4Rasters.children[currentRaster].data.name);
                a4rectangle = objectToPaperRectangle(rectangles[a4Rasters.children[currentRaster].data.name], a4Rasters.children[currentRaster] as paper.Raster);
                updateA4Shape();
                a4Rasters.children[currentRaster].visible = true;
            }
            break;
        case "Right":
        case "ArrowRight":
            if (currentRaster < a4Rasters.children.length - 1) {
                a4Rasters.children[currentRaster].visible = false;
                currentRaster++;
                console.log(a4Rasters.children[currentRaster].data.name);
                a4rectangle = objectToPaperRectangle(rectangles[a4Rasters.children[currentRaster].data.name], a4Rasters.children[currentRaster] as paper.Raster);
                updateA4Shape();
                a4Rasters.children[currentRaster].visible = true;
            }
            break;
        case "Enter":
            rectangles[a4Rasters.children[currentRaster].data.name] = paperRectangleToObject(a4rectangle, a4Rasters.children[currentRaster] as paper.Raster);
            a4shape.strokeColor = 'orange' as any;
            setTimeout(() => a4shape.strokeColor = 'green' as any, 500);
            localStorage.setItem('rectangles', JSON.stringify(rectangles));
            break;
        case "Esc":
        case "Escape":
            break;
        case "s":
            download(JSON.stringify(rectangles, null, '\t'), 'rectangles.json', 'application/json');
            break;
        case "l":
            loadRectanglesButton.click();
            break;
        case "g":
            draw(a4rectangle);
            break;
        case "e":
            exportImages();
            break;
        default:
            return;
    }
};

let onKeyUp = () => {
};


document.addEventListener('keydown', onKeyDown, false);
document.addEventListener('keyup', onKeyUp, false);

let loadRectanglesButton = document.getElementById("load-rectangles") as HTMLInputElement;
loadRectanglesButton.addEventListener("change", function () {
    var file_to_read = (loadRectanglesButton as any).files[0];
    var fileread = new FileReader();
    fileread.onload = function (e: any) {
        var content = e.target.result;
        rectangles = JSON.parse(content);
        localStorage.setItem('rectangles', JSON.stringify(rectangles));
    };
    fileread.readAsText(file_to_read);
});